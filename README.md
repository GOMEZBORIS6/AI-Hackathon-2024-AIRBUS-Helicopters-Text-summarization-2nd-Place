# AI-Hackathon-2024-AIRBUS-Helicopters-Text-summarization
Create a model designed to succinctly summarize segments of legal texts provided by the Digital Acceleration Office of Airbus. This model's effectiveness will be determined through a range of metrics such as the Rouge metric, similarity score, and the originality/relevance of the approach, among others.
<br>
We took ***second place in this hackathon***, achieving a ***performance of 80%***, almost identical to that of the winning team, apart from one minor detail.
